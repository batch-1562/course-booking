import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Update Course Page',
	content: 'Modify product details below.'
}

export default function UpdateCourse () {

	const addCourse = (eventSubmit) => {
		eventSubmit.preventDefault();
		return(
			Swal.fire({
				icon: "success",
				title: 'Course Update Successful!',
				text: 'New course details have been saved.'
			})
		);
	};

	return (
		<div>
			<Hero bannerData={data}/>
			
			<Container>
				<h1 className="text-center">Update Course Form</h1>
				<Form onSubmit={e => addCourse(e)}>
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" required />
					</Form.Group>

					<Form.Group>
						<Form.Check type="switch" required id="custom-switch" label="Enable Course"/>
					</Form.Group>
					<Button className="btn-primary btn-block mt-4" type="submit">Submit</Button>
				</Form>
			</Container>
		</div>	
	);
};